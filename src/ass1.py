import nltk
import urllib.request
from bs4 import BeautifulSoup
from nltk import word_tokenize
from nltk import re
from nltk.tag import pos_tag
from nltk.stem import WordNetLemmatizer


def tag_words(words_to_tag):
    tagged_words = pos_tag(words_to_tag)
    return tagged_words


def recursive_list_item_removal(area):
    extracted_text = ""
    for list_item in area(["li"]):
        extracted_text = extracted_text + recursive_list_item_removal(list_item)
        extracted_text = extracted_text + list_item.get_text() + " "
        list_item.extract()
        # print(extracted_text)
    return extracted_text


def tokenize_text(text_to_tokenize):
    tokens =  [word for word in word_tokenize(text_to_tokenize)
                  if re.search("\w", word)]
    return tokens


def generate_lemmatized_types(extracted_tokens):
    lemmatized_types_to_return = []
    lemmatizer = WordNetLemmatizer()

    for word in extracted_tokens:
        lem_word = lemmatizer.lemmatize(word)
        lemmatized_types_to_return.append(lem_word)

    return set(lemmatized_types_to_return)


def process_url(url):
    # Step - HTML Parsing
    url_response = urllib.request.urlopen(
        url)
    html = url_response.read()
    soup = BeautifulSoup(html, "html5lib")

    # From Stackoverflow - Python, remove all html tags
    # (and scripts): https://stackoverflow.com/questions/37018475/python-remove-all-html-tags-from-string
    for script in soup(["script", "style"]):
        script.extract()

    body = soup.find("body")

    # get text
    extracted_text = recursive_list_item_removal(body)
    text = soup.text.strip()

    # break into lines and remove leading and trailing space on each
    lines = (line.strip() for line in text.splitlines())
    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    # drop blank lines
    extracted_text = extracted_text + "\n".join(chunk for chunk in chunks if chunk)

    print(extracted_text)

    # Step - Pre-processing: Sentence Splitting, Tokenization and Normalization

    # Sentence Splitting
    sentences = extracted_text.split(".")
    print("Sentences")
    for sentence in sentences:
        print("• {}".format(sentence))

    # Tokenization
    extracted_tokens = tokenize_text(extracted_text)
    lemmatized_types = generate_lemmatized_types(extracted_tokens)
    tagged_words = tag_words(lemmatized_types)
    print(tagged_words)

url_string = input("Enter URLs: ")
url_array = url_string.split(',')

for url_address in url_array:
    print(url_address)
    process_url(url_address)


